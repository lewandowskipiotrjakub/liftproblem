﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LiftProblem.Domain
{
    public enum LiftAction
    {
        UP,
        DOWN,
        OPEN,
        CLOSE,
    }
}
