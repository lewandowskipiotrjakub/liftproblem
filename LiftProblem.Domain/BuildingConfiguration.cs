﻿using System.Collections.Generic;

namespace LiftProblem.Domain
{
    public class BuildingConfiguration
    {
        public List<int> FloorsAvailableForLiftRequest { get; set; }
    }
}