﻿using System;
using System.Collections.Generic;

namespace LiftProblem.Domain
{
    public class Lift
    {
        public int Floor { get; set; }
        public bool IsOpened { get; set; }
        public IList<LiftRequest> Humans { get; set; }
        public int Capacity { get; }
        public IList<int> AvailableFloors { get; }
    }
}
