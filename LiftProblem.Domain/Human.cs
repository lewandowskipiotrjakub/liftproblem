﻿namespace LiftProblem.Domain
{
    public class LiftRequest
    {
        public int Id { get; internal set; }
        public Trip Trip { get; set; }
    }
}