﻿using System.Collections.Generic;

namespace LiftProblem.Domain
{
    public class LiftState
    {
        public int CurrentFloor { get; set; }
        public LiftStateOperation LiftStateOperation { get; set; }
        public bool IsBusy { get; set; }
        public IEnumerable<int> HumanTripEnds {get; set; }
    }

    public enum LiftStateOperation
    {
        UP,
        DOWN,
        OPENING,
        CLOSING,
        IDLE
    }
}