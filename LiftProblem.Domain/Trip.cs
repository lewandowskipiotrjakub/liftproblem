﻿namespace LiftProblem.Domain
{
    public class Trip
    {
        public int StartFloor { get; }
        public int EndFloor { get; }
        public Trip(int startFloor, int endFloor)
        {
            StartFloor = startFloor;
            EndFloor = endFloor;
        }
    }
}