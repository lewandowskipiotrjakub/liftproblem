﻿using LiftProblem.Domain;
using System;

namespace LiftProblem.Logic.Controllers
{
    public interface ILiftActionStrategyFactory
    {
        ILiftActionStrategy GetStrategy(LiftAction liftAction);
    }


    public class LiftActionStrategyFactory : ILiftActionStrategyFactory
    {
        public ILiftActionStrategy GetStrategy(LiftAction liftAction)
        {
            switch (liftAction)
            {
                case LiftAction.UP:
                        return new UpActionStrategy();                    
                case LiftAction.DOWN:
                    return new DownActionStrategy();
                case LiftAction.OPEN:
                    return new OpenActionStrategy();
                case LiftAction.CLOSE:
                    return new CloseActionStrategy();
                default:
                    throw new InvalidOperationException("TODO");
            }
        }
    }
}
