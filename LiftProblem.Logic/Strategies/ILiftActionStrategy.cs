﻿using LiftProblem.Logic.Services;

namespace LiftProblem.Logic.Controllers
{
    public interface ILiftActionStrategy
    {
        void Invoke(ILiftOperator liftOperator);
    }
}
