﻿namespace LiftProblem.Domain
{
    public enum LiftExceptionErrorCode
    {
        CANT_OPEN_OPENED_LIFT = 1,
        CANT_CLOSE_CLOSED_LIFT = 2,
        CANT_ADD_HUMAN = 3,
        CANT_RELEASE_HUMAN = 4,
        CANT_GO_DOWN = 5,
    }
}