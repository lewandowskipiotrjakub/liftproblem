﻿using System;
using System.Runtime.Serialization;

namespace LiftProblem.Domain
{
    [Serializable]
    internal class LiftException : Exception
    {
        public LiftException()
        {
        }

        public LiftException(LiftExceptionErrorCode liftExceptionErrorCode) : base()
        {
        }

        public LiftException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LiftException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}