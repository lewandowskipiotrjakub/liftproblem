﻿using LiftProblem.Domain;
using System.Collections.Generic;

namespace LiftProblem.Logic.Controllers
{
    public interface ILiftRequestsProvider
    {
        IEnumerable<LiftRequest> GetNewLiftRequests();
    }
}