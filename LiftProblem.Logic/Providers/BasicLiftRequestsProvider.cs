﻿using LiftProblem.Domain;
using System;
using System.Collections.Generic;

namespace LiftProblem.Logic.Controllers
{
    public class BasicLiftRequestsProvider : ILiftRequestsProvider
    {
        private readonly BuildingConfiguration _buildingConfiguration;

        static int TickCounter = 0;

        public BasicLiftRequestsProvider(BuildingConfiguration buildingConfiguration)
        {
            _buildingConfiguration = buildingConfiguration;
        }

        public IEnumerable<LiftRequest> GetNewLiftRequests()
        {
            TickCounter++;
            if (TickCounter % 4 == 0)
            {
                var randomLiftRequest = GetNewRandomLiftRequest();
                return new List<LiftRequest>()
                {
                    randomLiftRequest
                };
            }
            else return new List<LiftRequest>();
             
        }

        private LiftRequest GetNewRandomLiftRequest()
        {
            var randomGenerator = new Random();

            var randomStartFloor = GetRandomElement(_buildingConfiguration.FloorsAvailableForLiftRequest);
            var randomEndFloor = GetRandomElement(_buildingConfiguration.FloorsAvailableForLiftRequest);


            return new LiftRequest()
            {
                Trip = new Trip(randomStartFloor, randomEndFloor)
            };
        }


        private int GetRandomElement(IList<int> list)
        {
            var rnd = new Random();
            var index = rnd.Next(list.Count);
            return list[index];
        }
    }
}