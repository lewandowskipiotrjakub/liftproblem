﻿using LiftProblem.Domain;

namespace LiftProblem.Logic.Validators
{
    public class LiftOperationValidator : ILiftOperationValidator
    {
        public bool CanAddHuman(Lift lift, LiftRequest human)
        {
            return lift.Humans.Count < lift.Capacity && human.Id != default(int);
        }

        public bool CanClose(Lift lift)
        {
            return lift.IsOpened;
        }

        public bool CanDown(Lift lift)
        {
            throw new System.NotImplementedException();
        }

        public bool CanOpen(Lift lift)
        {
            return !lift.IsOpened;
        }

        public bool CanReleaseHuman(Lift lift, LiftRequest human)
        {
            return lift.Humans.Contains(human);
        }

        public bool CanUp(Lift lift)
        {
            return isUpperFloorAvailable(lift);
        }

        private bool isUpperFloorAvailable(Lift lift)
        {
            var upperFloor = lift.Floor + 1;
            return lift.AvailableFloors.Contains(upperFloor);
        }

        private bool isBottomFloorAvailable(Lift lift)
        {
            var bottomFloor = lift.Floor -1;
            return lift.AvailableFloors.Contains(bottomFloor);
        }
    }
}