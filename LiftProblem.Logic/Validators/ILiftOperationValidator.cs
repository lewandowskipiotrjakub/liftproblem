﻿using LiftProblem.Domain;

namespace LiftProblem.Logic.Validators
{
    public interface ILiftOperationValidator
    {
        bool CanUp(Lift lift);
        bool CanDown(Lift lift);
        bool CanAddHuman(Lift lift, LiftRequest human);
        bool CanReleaseHuman(Lift lift, LiftRequest human);
        bool CanClose(Lift lift);
        bool CanOpen(Lift lift);
    }
}