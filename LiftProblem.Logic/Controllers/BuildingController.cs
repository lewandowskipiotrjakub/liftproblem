﻿using LiftProblem.Domain;
using LiftProblem.Logic.Alghorytms;
using System.Collections.Generic;

namespace LiftProblem.Logic.Controllers
{
    public class BuildingController
    {
        private IEnumerable<LiftController> liftControllers;
        private readonly ILiftAlghorytm _liftAlghorytm;
        private readonly ILiftRequestsProvider _liftRequestsProvider;
        private readonly BuildingConfiguration _buildingConfiguration;

        public void PerformAllLiftsOperations()
        {
           var humansWaiting = _liftRequestsProvider.GetNewLiftRequests();
            
           var liftStates = GetAllLiftStates();

           var alghorytmResult = _liftAlghorytm.CalculateNextOperations(liftStates, humansWaiting);

           ApplyAlghorytmResult(alghorytmResult);
        }

        private void ApplyAlghorytmResult(ILiftAlghorytmResult alghorytmResult)
        {
            foreach (var liftController in liftControllers)
            {
                var nextLiftAction = alghorytmResult.GetNextLiftAction(liftController.Id);                
                liftController.PerformLiftAction(nextLiftAction);
            }
        }

        private IEnumerable<LiftState> GetAllLiftStates()
        {
            foreach (var liftController in liftControllers)
            {
                yield return liftController.GetLiftState();
            }
        }
    }
}
