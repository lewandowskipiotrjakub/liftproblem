﻿using LiftProblem.Domain;
using LiftProblem.Logic.Services;

namespace LiftProblem.Logic.Controllers
{
    public class LiftController
    {
        private readonly ILiftOperator _liftOperator;
        private readonly ILiftActionStrategyFactory _liftActionStrategyFactory;
        public int Id { get; set; }
        public LiftController(ILiftOperator liftOperator, ILiftActionStrategyFactory liftActionStrategyFactory)
        {
            _liftOperator = liftOperator;
            _liftActionStrategyFactory = liftActionStrategyFactory;
        }

        public void PerformLiftAction(LiftAction liftAction)
        {
            var liftActionStrategy = _liftActionStrategyFactory.GetStrategy(liftAction);
            liftActionStrategy.Invoke(_liftOperator);
        }

        public LiftState GetLiftState()
        {
            return _liftOperator.GetState();
        }
    }
}
