﻿using LiftProblem.Domain;
using LiftProblem.Logic.Validators;

namespace LiftProblem.Logic.Services
{
    public class LiftOperator : ILiftOperator
    {
        private readonly ILiftOperationValidator _liftOperationValidator;
        private readonly Lift _lift;
        public LiftOperator(Lift lift, ILiftOperationValidator liftOperationValidator)
        {
            _liftOperationValidator = liftOperationValidator;
            _lift = lift;
        }

        public void AddHuman(LiftRequest human)
        {
            if (_liftOperationValidator.CanAddHuman(_lift, human))
                _lift.Humans.Add(human);
            else throw new LiftException(LiftExceptionErrorCode.CANT_ADD_HUMAN);
        }

        public void Close()
        {
            if (_liftOperationValidator.CanClose(_lift))
                _lift.IsOpened = false;
            else throw new LiftException(LiftExceptionErrorCode.CANT_CLOSE_CLOSED_LIFT);

        }

        public void Down()
        {
            if (_liftOperationValidator.CanDown(_lift))
                _lift.Floor--;
            else throw new LiftException(LiftExceptionErrorCode.CANT_GO_DOWN);
        }

        public LiftState GetState()
        {
            return new LiftState
            {
                CurrentFloor = _lift.Floor,
                IsBusy = false,
                LiftStateOperation = LiftStateOperation.IDLE
                //TODO jesli bede pisał modul tickow to tutaj bedzie cos innego niz idle
            };
        }

        public void Open()
        {
            if (_liftOperationValidator.CanOpen(_lift))
                _lift.IsOpened = true;
            else throw new LiftException(LiftExceptionErrorCode.CANT_OPEN_OPENED_LIFT);
        }

        public void ReleaseHuman(LiftRequest human)
        {
            if (_liftOperationValidator.CanReleaseHuman(_lift, human))
                _lift.Humans.Remove(human);
            else throw new LiftException(LiftExceptionErrorCode.CANT_RELEASE_HUMAN);
        }

        public void Up()
        {
            if (_liftOperationValidator.CanUp(_lift))
                _lift.Floor++;
            else throw new LiftException(LiftExceptionErrorCode.CANT_GO_DOWN);
        }
    }
}
