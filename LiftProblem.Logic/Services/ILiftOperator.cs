﻿using LiftProblem.Domain;

namespace LiftProblem.Logic.Services
{
    public interface ILiftOperator
    {
        void Up();
        void Down();
        void Open();
        void Close();
        void AddHuman(LiftRequest human);
        void ReleaseHuman(LiftRequest human);
        LiftState GetState();
    }
}
