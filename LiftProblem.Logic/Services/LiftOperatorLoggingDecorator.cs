﻿using LiftProblem.Domain;
using LiftProblem.Logic.Validators;

namespace LiftProblem.Logic.Services
{
    public class LiftOperatorLoggingDecorator : LiftOperator
    {
        private LiftOperator _liftOperator;
        public LiftOperatorLoggingDecorator(Lift lift, ILiftOperationValidator liftOperationValidator) : base(lift,liftOperationValidator)
        {
            _liftOperator = new LiftOperator(lift, liftOperationValidator);
        }

        //todo make overrides 

    }
}
