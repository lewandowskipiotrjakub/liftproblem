﻿using System;
using System.Runtime.Serialization;

namespace LiftProblem.Logic.Alghorytms
{
    [Serializable]
    internal class LiftAlghorytmException : Exception
    {
        public LiftAlghorytmException()
        {
        }

        public LiftAlghorytmException(LiftAlghorytmErrorCode liftAlghorytmErrorCode)
        {
        }

        public LiftAlghorytmException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LiftAlghorytmException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}