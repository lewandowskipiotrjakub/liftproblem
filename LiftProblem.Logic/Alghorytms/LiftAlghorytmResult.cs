﻿using LiftProblem.Domain;
using System.Collections.Generic;

namespace LiftProblem.Logic.Alghorytms
{
    public class LiftAlghorytmResult : ILiftAlghorytmResult
    {
        public IDictionary<int, LiftAction> NextLiftOperations { get; set; }
        public LiftAction GetNextLiftAction(int liftId)
        {
            if (NextLiftOperations.TryGetValue(liftId, out LiftAction liftAction))
                return liftAction;
            else
                throw new LiftAlghorytmException(LiftAlghorytmErrorCode.NULL_LIFT_ACTION);
        }
    }
}