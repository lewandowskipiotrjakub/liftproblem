﻿using LiftProblem.Domain;

namespace LiftProblem.Logic.Alghorytms
{
    public interface ILiftAlghorytmResult
    {
        LiftAction GetNextLiftAction(int liftId);
    }
}