﻿using System.Collections.Generic;
using LiftProblem.Domain;

namespace LiftProblem.Logic.Alghorytms
{
    public interface ILiftAlghorytm
    {
        LiftAlghorytmResult CalculateNextOperations(IEnumerable<LiftState> liftStates, IEnumerable<LiftRequest> humansWaitingForLift);
    }
}